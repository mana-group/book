<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'],function () {

    Route::post('/login','AuthController@login');

    Route::post('/register','AuthController@register');

    Route::get('/poetlist','PoetController@poetlist');

    Route::get('/poet/categories/{id}','CategoryController@category');

    Route::get('/category/poetries/{id}','PoetryController@poetryList');

    Route::get('/poetry/media/{id}','PoetryController@poetryMedia');

    Route::get('/comment','CommentController@comment');


    Route::get('/follower/{id}','HomeController@followerList');

    Route::get('/following/{id}','HomeController@followingList');

    Route::group(['middleware' => 'auth:api'],function (){

        Route::get('/myprofile','HomeController@myProfile');

        Route::get('/home','HomeController@home');

        Route::any('/editprofile','HomeController@editProfile');

        Route::post('/poetry/mediacreate','MediaController@mediaCreate');

        Route::get('/notification','HomeController@notification');

        Route::post('/comment','CommentController@comment');

        Route::post('/follow','HomeController@follow');

        Route::post('/like/{id}','LikeController@like');

        Route::get('/userprofile/{id}','HomeController@userProfile');



    });
});