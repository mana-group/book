<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace' => 'Web'],function (){

    Route::any('/','AuthController@login')->name('login');

    Route::get('/home','HomeController@home')->name('home');

    Route::any('/login', 'AuthController@login')->name('login');

    Route::get('/logout', 'AuthController@logout')->name('logout');

    Route::middleware('auth')->group(function (){

        Route::middleware('role:admin')->group(function () {

    Route::group(['prefix' => 'poet'], function () {

        Route::any('/create','PoetController@create');

        Route::get('/list','PoetController@list');

        Route::get('/delete/{id}','PoetController@delete');

        Route::any('/edit/{id}','PoetController@edit');

        });

    Route::group(['prefix' => 'category'],function (){

        Route::any('/create/{id}','CategoryController@create');

        Route::get('/list/{id}','CategoryController@list');

        Route::get('/delete/{id}','CategoryController@delete');

        Route::any('/edit/{id}','CategoryController@edit');
    });

    Route::group(['prefix' => 'poetry'],function (){

        Route::any('/create/{id}','PoetryListController@create');

        Route::get('/list/{id}','PoetryListController@list');

        Route::get('/delete/{id}','PoetryListController@delete');

        Route::any('/edit/{id}','PoetryListController@edit');
    });

    Route::group(['prefix' => 'media'],function (){

        Route::any('/create/{id}','PoetryController@create');

        Route::get('/list/{id}','PoetryController@list');

        Route::get('/delete/{id}','PoetryController@delete');

        Route::any('/edit/{id}','PoetryController@edit');
    });
       Route::get('/comment/list/{id}','CommentController@list');

       Route::get('/like/list/{id}','LikeController@list');
    });

    });


});



