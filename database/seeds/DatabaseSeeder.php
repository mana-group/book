<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $role = new \App\Role();
        $role->name = 'admin';
        $role->display_name = 'Admin';
        $role->save();

        $role = new \App\Role();
        $role->name = 'user';
        $role->display_name = 'User';
        $role->save();

        $user = new \App\User();
        $user->name = "admin";
        $user->family = "admin zade";
        $user->username = 'admin';
        $user->password = bcrypt("12345");
        $user->save();
        $role = App\Role::where("name", "admin")->first();
        $user->attachRole($role);

//        $user = new \App\User();
//        $user->name = "mina";
//        $user->family = "bazregary";
//        $user->username = 'mina';
//        $user->password = bcrypt("12345");
//        $user->email = 'mina.bazregary60@gmail.com';
//        $user->image = '';
//        $user->gender = '2';
//        $user->message = '';
//        $user->biography = '';
//        $user->save();
//        $role = App\Role::where("name", "user")->first();
//        $user->attachRole($role);
//
//        $user = new \App\User();
//        $user->name = "bita";
//        $user->family = "farahi";
//        $user->username = 'bita';
//        $user->password = bcrypt("12345");
//        $user->email = 'bita.farahi@gmail.com';
//        $user->image = '';
//        $user->gender = '2';
//        $user->message = '';
//        $user->biography = '';
//        $user->save();
//        $role = App\Role::where("name", "user")->first();
//        $user->attachRole($role);
//
//        $user = new \App\User();
//        $user->name = "ali";
//        $user->family = "irani";
//        $user->username = 'ali';
//        $user->password = bcrypt("12345");
//        $user->email = 'ali.irani@gmail.com';
//        $user->image = '';
//        $user->gender = '1';
//        $user->message = '';
//        $user->biography = '';
//        $user->save();
//        $role = App\Role::where("name", "user")->first();
//        $user->attachRole($role);
//
//        $user = new \App\User();
//        $user->name = "iman";
//        $user->family = "ahmadi";
//        $user->username = 'iman';
//        $user->password = bcrypt("12345");
//        $user->email = 'iman.ahmadi@gmail.com';
//        $user->image = '';
//        $user->gender = '1';
//        $user->message = '';
//        $user->biography = '';
//        $user->save();
//        $role = App\Role::where("name", "user")->first();
//        $user->attachRole($role);

//        comment
        $comment = new \App\Comment();
        $comment->user_id = '2';
//        $comment->poetry_id = '1';
        $comment->media_id = '1';
        $comment->seen = '';
        $comment->description = '';
        $comment->save();

        $comment = new \App\Comment();
        $comment->user_id = '2';
        $comment->media_id = '2';
        $comment->seen = '';
        $comment->description = '';
        $comment->save();

        $comment = new \App\Comment();
        $comment->user_id = '2';
        $comment->media_id = '3';
        $comment->seen = '';
        $comment->description = '';
        $comment->save();

//        like
//        $like = new \App\Like();
//        $like->user_id = '2';
//        $like->poet_id = '1';
//        $like->category_id = '1';
//        $like->poetry_id = '1';
//        $like->media_id = '1';
//        $like->save();
//
//        $like = new \App\Like();
//        $like->user_id = '2';
//        $like->poet_id = '2';
//        $like->category_id = '2';
//        $like->poetry_id = '2';
//        $like->media_id = '2';
//        $like->save();
//
//        $like = new \App\Like();
//        $like->user_id = '2';
//        $like->poet_id = '3';
//        $like->category_id = '3';
//        $like->poetry_id = '3';
//        $like->media_id = '3';
//        $like->save();

//        poet
        $poet = new \App\Poet();
        $poet->name = 'saedi';
        $poet->authorName = 'saedi';
        $poet->image = '';
        $poet->format = 'gasideh';
        $poet->save();

        $poet = new \App\Poet();
        $poet->name = 'hafez';
        $poet->authorName = 'hafez';
        $poet->image = '';
        $poet->format = 'gazal';
        $poet->save();

        $poet = new \App\Poet();
        $poet->name = 'khayam';
        $poet->authorName = 'khayam';
        $poet->image = '';
        $poet->format = 'robaei';
        $poet->save();

//        category
        $category = new \App\Category();
        $category->poet_id = '1';
        $category->name = 'golestan';
        $category->image = '';
        $category->save();

        $category = new \App\Category();
        $category->poet_id = '1';
        $category->name = 'boostan';
        $category->image = '';
        $category->save();

//        poetry
        $poetry = new \App\Poetry();
        $poetry->category_id = '1';
        $poetry->name = 'dar sirat padeshahan';
        $poetry->text = '';
        $poetry->save();

        $poetry = new \App\Poetry();
        $poetry->category_id = '1';
        $poetry->name = 'dar akhlagh darvishan';
        $poetry->text = '';
        $poetry->save();

        $poetry = new \App\Poetry();
        $poetry->category_id = '1';
        $poetry->name = 'fazilat ghenaat';
        $poetry->text = '';
        $poetry->save();

//        media
        $media = new\App\Media();
        $media->user_id = '2';
        $media->poetry_id = '1';
        $media->type = '1';
        $media->link = '1';
        $media->save();

        $media = new\App\Media();
        $media->user_id = '2';
        $media->poetry_id = '1';
        $media->type = '2';
        $media->link = '2';
        $media->save();

        $media = new\App\Media();
        $media->user_id = '2';
        $media->poetry_id = '1';
        $media->type = '3';
        $media->link = '3';
        $media->save();

//        follow
        $follow = new\App\Follow();
        $follow->follower = '2';
        $follow->following = '3';
        $follow->status = '2';
        $follow->save();

        $follow = new\App\Follow();
        $follow->follower = '2';
        $follow->following = '4';
        $follow->status = '2';
        $follow->save();

        $follow = new\App\Follow();
        $follow->follower = '3';
        $follow->following = '4';
        $follow->status = '2';
        $follow->save();

    }
}
