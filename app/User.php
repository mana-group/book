<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',  'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function media()
    {
        return $this->hasMany("App\Media");
    }

    public function comment()
    {
        return $this->belongsTo("App\Comment");
    }

    public function like()
    {
        return $this->belongsTo("App\Like");
    }

    public function followings()
    {
        return $this->belongsToMany('App\User', 'follow','follower',"id");
    }
    public function followers()
    {
        return $this->belongsToMany('App\User', 'follow','following',"id");
    }
    public function getFollowerCountAttribute()
    {
        return $this->attributes["follower_count"] = $this->followers()->count();
    }
    public function getFollowingCountAttribute()
    {
        return $this->attributes["following_count"] = $this->followings()->count();
    }

}
