<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function poet()
    {
        return $this->belongsTo("App\Poet");
 }

    public function poetries()
    {
        return $this->hasMany("App\Poetry");
    }
    public function likes()
    {
        return $this->hasMany("App\Like");
    }
    public function getLikesCountAttribute()
    {
        return $this->attributes["likes_count"] = $this->likes()->count();

    }
}
