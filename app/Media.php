<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public function poetry()
    {
        return $this->belongsTo("App\Poetry");
    }
    public function user()
    {
        return $this->belongsTo("App\User");
    }
    public function likes()
    {
        return $this->hasMany("App\Like");
    }
    public function comments()
    {
        return $this->hasMany("App\Comment");
    }
    public function getLikesCountAttribute()
    {
        return $this->attributes["likes_count"] =$this->likes()->count();
    }
    public function getCommentsCountAttribute()
    {
        return $this->attributes["comments_count"] =$this->comments()->count();
    }
}
