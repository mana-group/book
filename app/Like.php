<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public function user()
    {
        return $this->belongsTo("App\User");
    }
    public function poet()
    {
        return $this->belongsTo("App\Poet");
    }
    public function poetry()
    {
        return $this->belongsTo("App\Poetry");
    }
    public function Category()
    {
        return $this->belongsTo("App\Category");
    }
    public function media()
    {
        return $this->belongsTo("App\Media");
    }
}
