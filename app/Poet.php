<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poet extends Model
{
    public function categories()
    {
         return $this->hasMany('App\Category');
    }
    public function likes()
    {
         return $this->hasMany('App\Like');
    }
    public function getLikesCountAttribute()
    {
        return $this->likes()->count();
    }
}
