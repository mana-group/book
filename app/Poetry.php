<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poetry extends Model
{
    public function category()
    {
        return $this->belongsTo("App\Category");
    }
    public function media()
    {
        return $this->hasMany("App\Media");
    }
    public function likes()
    {
        return $this->hasMany('App\Like');
    }
    public function getLikesCountAttribute()
    {
        return $this->likes()->count();
    }
    public function comments()
    {
         return $this->hasMany('App\Comment');
    }
    public function getCommentsCountAttribute()
    {
        return $this->comments()->count();
    }
}
