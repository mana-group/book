<?php

namespace App\Http\Controllers\Web;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/20/2019
     * Time : 12:39 PM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request,$id)
    {
        $media = Media::findOrFail($id);
        $comments = $media->comments()->get();
        return view('comment.list',compact('comments','id'));
//        $comments = Comment::all();
//        return view('comment.list',compact('comments'));
   }
}
