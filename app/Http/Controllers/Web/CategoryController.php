<?php

namespace App\Http\Controllers\Web;

use App\Category;
use App\Http\Controllers\Controller;
use App\Multimedia;
use App\Poet;
use Illuminate\Http\Request;
use Validator;
use DB;

class CategoryController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 10/16/2019
     * Time : 2:31 PM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|string
     */
    public function create(Request $request, $id)
    {
        if ($request->isMethod("get")) {
            $poet = Poet::findOrFail($id);
            return view('category.create', compact('poet'));
        } elseif ($request->isMethod("post")) {
            $valid = Validator::make($request->all(), [
                'name' => 'required'
            ]);
            if ($valid->fails()) {
                return back()->withErrors($valid);
            }
            DB::beginTransaction();
                try {
                    $category = new Category();
                    if ($request->hasFile('image')) {
                        $image = $request->file('image');
                        $image_name = $request->id . "_" . $request->name . "_" . time() . "." . $image->getClientOriginalExtension();
                        $destination_path = 'files/category';
                        $image->move($destination_path, $image_name);
                        $category->image = '/' . $image_name;
                    }
                $category->name = $request->name;
                $category->poet_id = $id;
                $category->save();
                DB::commit();
                return redirect('/category/list/' . $id);

//
            } catch (\Exception $exception) {
                DB::rollBack();
                return $exception->getMessage();
                return back()->with('error', 'خطایی در سرور رخ داده است');
            }
        }
    }

    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 10/16/2019
     * Time : 2:30 PM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function list(Request $request, $id)
    {
        $poet = Poet::findOrFail($id);
        $categories = $poet->categories()->get();
        return view('category.list', compact('categories', 'poet'));
    }

    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/17/2019
     * Time : 12:00 PM
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return back()->with('success', 'با موفقیت حذف شد');
    }

    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/18/2019
     * Time : 9:39 AM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        if ($request->isMethod("get")) {
            $category = Category::findOrFail($id);
            return view('category.create', compact('category'));
        } elseif ($request->isMethod("post")) {
            $category = Category::findOrFail($id);
            $valid = Validator::make($request->all(), [
                'name' => 'required'
            ]);
            if ($valid->fails()) {
                return back()->withErrors($valid);
            }
            $category->name = $request->name;
            $category->save();
            return redirect('/category/list/'.$category->poet_id)->with('success', 'ویرایش با موفقیت انجام شد');
        } else {
            abort('404');
        }
    }
}
