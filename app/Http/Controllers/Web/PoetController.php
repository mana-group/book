<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Poet;
use Illuminate\Http\Request;
use function Sodium\crypto_box_publickey_from_secretkey;
use Validator;
use DB;

class PoetController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 10/15/2019
     * Time : 1:12 PM
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        if ($request->isMethod("get")) {
            return view('poet.create');
        } elseif ($request->isMethod("post")) {
            $valid = Validator::make($request->all(), [
                'name' => 'required',
                'authorName' => 'required',
                'format' => 'required'
            ]);
            if ($valid->fails()) {
                return back()->withErrors($valid);
            }
            DB::beginTransaction();
            try {
                $poet = new Poet();
                if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $image_name = $request->id . "_" . $request->name . "_" . time() . "." . $image->getClientOriginalExtension();
                    $destination_path = 'files/poets';
                    $image->move($destination_path, $image_name);
                    $poet->image = '/' . $image_name;
                }
                $poet->name = $request->name;
                $poet->authorName = $request->authorName;
                $poet->format = $request->format;
                $poet->save();
                DB::commit();
                return back()->with('success', 'با موفقیت اضافه شد');
            } catch (\Exception $exception) {
                DB::rollBack();
                return back()->with('error', 'خطایی در سرور رخ داده است');
            }
        }

    }

    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 10/16/2019
     * Time : 9:57 AM
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request)
    {
        $poets = Poet::all();
        return view('poet.list', compact('poets'));
    }

    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 10/16/2019
     * Time : 9:57 AM
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function delete($id)
    {
        $poets = Poet::findOrFail($id);
        $poets->delete();
        return back()->with('success', 'حذف با موفقیت انجام شد');
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 10/16/2019
     * Time : 10:17 AM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        if ($request->isMethod("get")) {
            $poet = Poet::findOrFail($id);
            return view('poet.create', compact("poet"));
        } elseif ($request->isMethod("post")) {
            $poet = Poet::findOrFail($id);
            $valid = Validator::make($request->all(), [
                'name' => 'required',
                'authorName' => 'required',
                'format' => 'required'
            ]);
            if ($valid->fails())
                return back()->withErrors($valid);
            $poet->name = $request->name;
            $poet->authorName = $request->authorName;
            $poet->format = $request->format;
            $poet->save();
            return redirect('poet/list')->with('success', 'ویرایش با موفقیت انجام شد');
        } else {
            abort('404');
        }
    }
}
