<?php

namespace App\Http\Controllers\web;

use App\Category;
use App\Http\Controllers\Controller;
use App\Poetry;
use Illuminate\Http\Request;
use Validator;
use DB;

class PoetryController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 10/30/2019
     * Time : 10:08 AM
     * @param Request $request
     * @param $id
     */
    public function create(Request $request, $id)
    {
        if ($request->isMethod("get")) {
            $category = Category::findOrFail($id);
            return view('poetry.create', compact('category'));
        } elseif ($request->isMethod("post")) {
            $valid = Validator::make($request->all(), [
                'name' => 'required',
//                'text' => 'required'
            ]);
            if ($valid->fails()) {
                return back()->withErrors($valid);
            }
            DB::beginTransaction();
            try {
                $poetry = new Poetry();
//                if ($request->hasFile('image')) {
//                    $image = $request->file('image');
//                    $image_name = $request->id . "_" . $request->name . "_" . time() . "." . $image->getClientOriginalExtension();
//                    $destination_path = 'files/poetry';
//                    $image->move($destination_path, $image_name);
//                    $poetry->image = '/' . $image_name;
//                }
                $poetry->name = $request->name;
                $poetry->text = $request->text;
                $poetry->category_id = $id;
                $poetry->save();
                DB::commit();
                return redirect('/poetry/list/'.$id )->with('success', 'با موفقیت اضافه شد');
            } catch (\Exception $exception) {
                DB::rollBack();
                return $exception;
                return back()->with('error', 'خطایی در سرور رخ داده است');
            }
        }
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 10/30/2019
     * Time : 12:45 PM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request,$id)
    {
        $category = Category::findOrFail($id);
        $poetries = $category->poetries()->get();
        return view('poetry.list',compact('poetries','category'));
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/18/2019
     * Time : 9:42 AM
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $poetry= Poetry::findOrFail($id);
        $poetry->delete();
        return back()->with('success','با موفقیت حذف شد');
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/18/2019
     * Time : 9:43 AM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        if ($request->isMethod("get")){
            $poetry = Poetry::findOrFail($id);
            return view('poetry.create',compact('poetry'));
        }elseif ($request->isMethod("post")){
            $poetry = Poetry::findOrFail($id);
            $valid = Validator::make($request->all(),[
                'name' => 'required',
                'text' => 'required'
            ]);
            if ($valid->fails()){
                return back()->withErrors($valid);
            }
            $poetry->name = $request->name;
            $poetry->text = $request->text;
            $poetry->save();
            return redirect('/poetry/list/'.$poetry->category_id)->with('success','ویرایش با موفقیت انجام شد');
        }else{
            abort('404');
        }
    }
}
