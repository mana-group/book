<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Media;
use App\Poetry;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/18/2019
     * Time : 1:23 PM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function create(Request $request, $id)
    {
        if ($request->isMethod("get")) {
            $poetry = Poetry::findOrFail($id);
            return view('media.create', compact('poetry'));
        } elseif ($request->isMethod("post")) {
            $valid = Validator::make($request->all(), [
                'type' => 'required',
                'file' => 'required',
                'text' => 'required'
            ]);
            if ($valid->fails()) {
                return back()->withErrors($valid);
            }
            DB::beginTransaction();
            try {
                $user = Auth::user();
                $media = new Media();
                if ($request->hasFile('file')) {
                    $file = $request->file('file');
                    $file_name = $request->id . "_" . $request->type . "_" . time() . "." . $file->getClientOriginalExtension();
                    $destination_path = 'files/media';
                    $file->move($destination_path, $file_name);
                    $media->link = '/' . $file_name;
                }
                $media->type = $request->type;
                $media->user_id = $user->id;
                $media->poetry_id = $id;
                $media->text = $request->text;
                $media->save();
                DB::commit();
                return redirect('/media/list/' . $id);
            } catch (\Exception $exception) {
                return $exception;
                DB::rollback();
                return back()->with('error', 'خطایی در سرور رخ داده است');
            }
        }
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/19/2019
     * Time : 11:12 AM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request, $id)
    {
        $poetry = Poetry::findOrFail($id);
        $media = $poetry->media()->get();
        return view('media.list', compact('media', 'poetry'));
    }

    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/20/2019
     * Time : 1:05 AM
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $media = Media::findOrFail($id);
        $media->delete();
        return back()->with('success', 'با موفقیت حذف شد');
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/20/2019
     * Time : 1:12 AM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit(Request $request, $id)
    {
        if ($request->isMethod("get")) {
            $media = Media::findOrFail($id);
            return view('media.create', compact('media'));
        } elseif ($request->isMethod("post")) {
            $media = Media::findOrFail($id);
            $valid = Validator::make($request->all(), [
                'type' => 'required'
            ]);
            if ($valid->fails()) {
                return back()->withErrors($valid);
            }
            $media->type = $request->type;
            $media->save();
            return redirect('/media/list/' . $media->poetry_id)->with('success', 'ویرایش با موفقیت انجام شد');
        } else {
            abort('404');
        }
    }
}
