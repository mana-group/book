<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if ($request->isMethod("get")){
            return view('auth.login');
        }elseif ($request->isMethod("post")){
            $user = User::where('username',$request->username)->whereRoleIs('admin')->first();
            if ($user && Hash::check($request->password ,$user->password)){
                Auth::login($user);
                return redirect('home');
            }else{
                return back()->with('error','نام کاربری یا رمز عبور صحیح نمی باشد');
            }
        }else {
            return abort('404');
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
