<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Media;
use App\like;

class LikeController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/20/2019
     * Time : 12:39 PM
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request,$id)
    {
        $media = Media::findOrFail($id);
        $likes = $media->likes()->get();
        return view('like.list',compact('likes','id'));
    }

}
