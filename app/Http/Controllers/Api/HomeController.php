<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Follow;
use App\Http\Controllers\Controller;
use App\Like;
use App\Media;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 2/28/2020
     * Time : 3:16 PM
     * @return \Illuminate\Http\JsonResponse
     */
    public function myProfile()
    {
        $myuser = Auth::user();
//        $following =$myuser -> followings()->get();
        $followings = Follow::where('follower', $myuser->id)->where('status', '1')->pluck('following');
        $media = Media::where('user_id', $myuser->id)->get();
        $myuser->append(['follower_count', 'following_count'])->toArray();
        return response()->json([
            'status' => 'success',
            'message' => ' my profile ',
            'path_user' => asset('/files/user'),
            'path_media' => asset('/files/media'),
            'myuser' => $myuser,
            'media' => $media
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }
    public function home()
    {
        $myuser = Auth::user();
//        $following =$myuser -> followings()->get();
        $followings = Follow::where('follower', $myuser->id)->where('status', '1')->pluck('following');
        $media = Media::whereIn('user_id', $followings)->get();
        return response()->json([
            'status' => 'success',
            'message' => ' home ',
            'path_user' => asset('/files/user'),
            'path_media' => asset('/files/media'),
            'media' => $media
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 2/28/2020
     * Time : 3:17 PM
     * @return \Illuminate\Http\JsonResponse
     */
    public function editProfile(Request $request)
    {
        if ($request->isMethod("get")) {
            $user = Auth::user();
            return response()->json([
                'status' => 'success',
                'message' => ' profile',
                'path' => asset('files/user'),
                'user' => $user
            ], 200,
                array('Content-Type' => 'application/json; charset=utf-8'),
                JSON_UNESCAPED_UNICODE);
        } elseif ($request->isMethod("post")) {
            $user = Auth::user();
            $valid = Validator::make($request->all(), [
                'name' => 'required',
                'username' => 'required|unique:users,id,' . $user->id,
                'biography' => 'required',
                'email' => 'required',
                'gender' => 'required',
            ]);
            if ($valid->fails())
                return response()->json([
                    'status' => 'failed',
                    'message' => $valid->errors()->first(),
                ],
                    422,
                    array('Content-Type' => 'application/json;charset:utf-8;'),
                    JSON_UNESCAPED_UNICODE
                );
            DB::beginTransaction();
            try {
                $user = Auth::user();
                if ($request->hasFile('image')) {
                    $image = $request->file('image');
                    $image_name = $request->id . "_" . $request->name . "_" . time() . "." . $image->getClientOriginalExtension();
                    $destination_path = 'files/user';
                    $image->move($destination_path, $image_name);
                    $user->image = '/' . $image_name;
                }
                $user->name = $request->name;
                $user->username = $request->username;
                $user->email = $request->email;
                $user->gender = $request->gender;
                if ($request->password)
                    $user->password = bcrypt($request->password);
                $user->biography = $request->biography;
                $user->save();
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' => ' ویرایش پروفایل با موفقیت انجام شد',
                ],
                    200,
                    array('Content-Type' => 'application/json;charset:utf-8;'),
                    JSON_UNESCAPED_UNICODE
                );
            } catch (\Exception $exception) {
                DB::rollBack();
                return response()->json([
                    'status' => 'failed',
                    'message' => $exception->getMessage(),
                ],
                    500,
                    array('Content-Type' => 'application/json;charset:utf-8;'),
                    JSON_UNESCAPED_UNICODE
                );
            }
        }
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 2/28/2020
     * Time : 3:17 PM
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile($id) {
        $myuser = Auth::user();
        $user = User::findOrFail($id);
        $media = $user->media()->get();
//        return $myuser;
        $follow = Follow::where('follower', $myuser->id)->where('status', '1')->where('following', $user->id)->first();
        if (!$follow)
            $follow_status = 0;
        else
            $follow_status = $follow->status;
        $user->append(['follower_count', 'following_count'])->toArray();
        return response()->json([
            'status' => 'success',
            'message' => ' user profile',
            'path' => asset('/files/user'),
            'path_media' => asset('/files/media'),
            'media' => $media,
            'user' => $user,
            'follow_status' => $follow_status
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 2/28/2020
     * Time : 3:17 PM
     * @return \Illuminate\Http\JsonResponse
     */
    public function notification(){
        $user = Auth::user();
        // likes
        $media = $user->media()->pluck('id');
        $likes = Like::whereIn('media_id', $media)->get();
//        //comments
        $comments = Comment::whereIn('media_id', $media)->get();
//        //follow
        $follows = Follow::where('following', $user->id)->where('status','1')->get();
        return response()->json([
            'status' => 'success',
            'message' => 'list notifications',
            'path_user' => asset('files/user'),
            'user' => $user,
            'likes' => $likes,
            'comments' => $comments,
            'follows' => $follows
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 3/1/2020
     * Time : 9:17 PM
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function follow(Request $request) {
        $valid = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if ($valid->fails())
            return response()->json([
                'status' => 'failed',
                'message' => $valid->errors()->first(),
            ],
                422,
                array('Content-Type' => 'application/json;charset:utf-8;'),
                JSON_UNESCAPED_UNICODE
            );
        $user = Auth::user();
        $req_user = User::findOrFail($request->user_id);
        $follow = Follow::where('follower', $user->id)->where('following', $req_user->id)->first();
        if ($follow) {
            if ($follow->status == '1') {
                $follow->status = '2';
                $msg = 'فالو لغو شد';
            } elseif ($follow->status == '2') {
                $follow->status = '1';
                $msg = 'فالو  شد';
            }
        } else {
            $follow = new Follow();
            $follow->follower = $user->id;
            $follow->following = $req_user->id;
            $follow->status = '1';
            $msg = 'فالو شد';
        }
        $follow->save();
        return response()->json([
            'status' => 'success',
            'message' => $msg,
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }


    public function followingList($id){
        $user = User::findOrFail($id);
        $following = Follow::where('follower', $user->id)->where('status', '1')->pluck('following');
        $users=User::whereIn('id',$following)->get();
        return response()->json([
            'status' => 'success',
            'message' => ' following list ',
//            'user' => $user,
            'following' => $users,
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }

    public function followerList($id){
        $user = User::findOrFail($id);
        $followers = Follow::where('following', $user->id)->where('status', '1')->pluck('follower');
        $users=User::whereIn('id',$followers)->get();
        return response()->json([
            'status' => 'success',
            'message' => ' follower list ',
//            'user' => $user,
            'followers' => $users,
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }
}
