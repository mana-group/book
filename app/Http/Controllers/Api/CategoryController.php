<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Poet;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/20/2019
     * Time : 1:27 AM
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function category($id)
    {
        $poet = Poet::findOrFail($id);
        $categories = $poet->categories()->get();
        foreach ($categories as $category) {
            $category->append(['likes_count'])->toArray();
        }
        return response()->json([
            'status' => 'success',
            'message' => ' poet categories!',
            'path_category' => asset('/files/category'),
            'categories' => $categories,
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }
}


