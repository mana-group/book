<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;

class MediaController extends Controller
{
    public function mediaCreate(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'poetry_id' => 'required',
            'link' => 'required|mimes:mp4,3gpp,quicktime,x-msvideo,x-ms-wmv,mpga,wav',
        ]);
        if ($valid->fails())
            return response()->json([
                'status' => 'failed',
                'message' => $valid->errors()->first(),
            ],
                422,
                array('Content-Type' => 'application/json;charset:utf-8;'),
                JSON_UNESCAPED_UNICODE
            );
        DB::beginTransaction();
        try {
            $user = Auth::user();
            $media = new Media();
            if ($request->hasFile('link')) {
                $file = $request->file('link');
                $file_name = $request->id . "_" . $user->id . "_" . time() . "." . $file->getClientOriginalExtension();
                $destination_path = 'files/media';
                $file->move($destination_path, $file_name);
                $media->link = '/' . $file_name;
            }
            if ($file->getClientOriginalExtension() == 'mp3' || $file->getClientOriginalExtension() == 'wav')
                $media->type = '1';
            else
                $media->type = '2';
            $media->user_id = $user->id;
            $media->poetry_id = $request->poetry_id;
            $media->save();
            DB::commit();
            return response()->json([
                'status' => 'success',
                'message' => ' رسانه با موفقیت اضافه شد',
            ],
                200,
                array('Content-Type' => 'application/json;charset:utf-8;'),
                JSON_UNESCAPED_UNICODE
            );
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'status' => 'failed',
                'message' => $exception->getMessage(),
            ],
                500,
                array('Content-Type' => 'application/json;charset:utf-8;'),
                JSON_UNESCAPED_UNICODE
            );
        }
    }
}
