<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Poet;
use Illuminate\Http\Request;

class PoetController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 10/30/2019
     * Time : 12:12 PM
     * @return \Illuminate\Http\JsonResponse
     */
    public function poetList() {
        $poets = Poet::all();
        foreach ($poets as $poet) {
            $poet->append(['likes_count'])->toArray();
        }
        return response()->json([
            'status' => 'success',
            'message' => 'poets',
            'path_poet' => asset('/files/poets'),
            'poets' => $poets

        ],200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }

}
