<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\Controller;
use App\Like;
use App\Poetry;
use Illuminate\Http\Request;

class PoetryController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 1/19/2020
     * Time : 10:19 AM
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function poetryList($id)
    {
        $category = Category::findOrFail($id);
        $poetries = $category->poetries()->get();
        foreach ($poetries as $poetry) {
            $poetry->append(['likes_count'])->toArray();
        }
        return response()->json([
            'status' => 'success',
            'message' => ' poetries list!',
            'path_poetry' => asset('/files/poetry'),
            'poetries' => $poetries,
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }

    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/20/2019
     * Time : 9:54 AM
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function poetryMedia($id)
    {
        $poetry = Poetry::findOrFail($id);
        $media = $poetry->media()->get();
        $user = auth('api')->user();
        foreach ($media as $me) {
            $me->append(['likes_count', 'comments_count'])->toArray();
            $me['is_like'] = 0;
            if ($user) {
                $is = Like::where('media_id', $me->id)->where('user_id', $user->id)->first();
//                return $is;
                if ($is)
                    $me['is_like'] = 1;
            }
        }
        return response()->json([
            'status' => 'success',
            'message' => ' poetry media!',
            'path_media' => asset('/files/media'),
            'media' => $media,
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }
}
