<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Media;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class CommentController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/20/2019
     * Time : 12:51 PM
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function comment(Request $request)
    {
        if ($request->isMethod("get")) {
            $valid = Validator::make($request->all(), [
                'media_id' => 'required',
            ]);
            if ($valid->fails())
                return response()->json([
                    'status' => 'failed',
                    'message' => $valid->errors()->first(),
                ],
                    422,
                    array('Content-Type' => 'application/json;charset:utf-8;'),
                    JSON_UNESCAPED_UNICODE
                );
            $media = Media::findOrFail($request->media_id);
            $comments = $media->comments()->get();
            foreach ($comments as $comment) {
                $user = User::findOrFail($comment->user_id);
                $comment['user'] = $user;
            }
            return response()->json([
                'status' => 'success',
                'message' => ' comments list!',
                'path_user' => asset('/files/user'),
                'comments' => $comments,
            ], 200,
                array('Content-Type' => 'application/json; charset=utf-8'),
                JSON_UNESCAPED_UNICODE);
        } elseif ($request->isMethod("post")) {
            $valid = Validator::make($request->all(), [
                'media_id' => 'required',
                'description' => 'required',
            ]);
            if ($valid->fails())
                return response()->json([
                    'status' => 'failed',
                    'message' => $valid->errors()->first(),
                ],
                    422,
                    array('Content-Type' => 'application/json;charset:utf-8;'),
                    JSON_UNESCAPED_UNICODE
                );
            $user = Auth::user();
            $comment = new Comment();
            $comment->user_id = $user->id;
            $comment->media_id = $request->media_id;
            $comment->description = $request->description;
            $comment->save();
            return response()->json([
                'status' => 'success',
                'message' => 'You Created New comment Successfully!',
            ],
                200,
                array('Content-Type' => 'application/json;charset:utf-8;'),
                JSON_UNESCAPED_UNICODE
            );
        } else
            abort(404);
    }
}