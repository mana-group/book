<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use http\Env\Response;
use DB;

class AuthController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/23/2019
     * Time : 11:06 AM
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        $valid = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);
        if ($valid->fails())
            return response()->json([
                'status' => 'fails',
                'message' => $valid->errors(),
            ],
                422,
                array('Content-Type' => 'application/json;charset:utf-8;'),
                JSON_UNESCAPED_UNICODE
            );
        $result = $request->only('username', 'password');
        $user = User::where('username', $request->username)->whereRoleIs('user')->first();
        if ($user) {
            if (!Hash::check($request->password, $user->password))
                return response()->json([
                    'status' => 'fail',
                    'message' => 'نام کاربری یا رمز عبور صحیح نمیباشد'
                ],
                    422,
                    array('Content-Type' => 'application/json;charset:utf-8;'),
                    JSON_UNESCAPED_UNICODE
                );
        } else {
            return response()->json([
                'status' => 'fail',
                'message' => 'کاربروجود ندارد'
            ],
                404,
                array('Content-Type' => 'application/json;charset:utf-8;'),
                JSON_UNESCAPED_UNICODE
            );
        }
        return response()->json([
            'status' => 'success',
            'message' => 'login successfully',
            'token' => $user->createToken('Mana')->accessToken,
        ], 200,
            array('Content-Type' => 'application/json;charset:utf-8;'),
            JSON_UNESCAPED_UNICODE
        );
    }
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 11/23/2019
     * Time : 11:06 AM
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'name' => 'required',
            'family' => 'required',
            'username' => 'required|unique:users,username',
            'password' => 'required'
        ]);
        if ($valid->fails())
            return response()->json([
                'status' => 'fails',
                'message' => $valid->errors()->first(),
            ],
                422,
                array('Content-Type' => 'application/json;charset:utf-8;'),
                JSON_UNESCAPED_UNICODE
            );
        $user = new User();
        $user->name = $request->name;
        $user->family = $request->family;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $role = Role::where('name','user')->first();
        DB::beginTransaction();
        try {
            $user->save();
            $user->attachRole($role);
            DB::commit();
        }catch (\Exception $exception){
            DB::rollback();
            return response()->json([
                'status' => 'failed',
                'message' => "خطایی در سرور رخ داده است!",
            ],
                500,
                array('Content-Type' => 'application/json;charset:utf-8;'),
                JSON_UNESCAPED_UNICODE
            );
        }
        return response()->json([
            'status' => 'success',
            'message' => 'ثبت نام انجام شد'
        ],
            200,
            array('Content-Type' => 'application/json;charset:utf-8;'),
            JSON_UNESCAPED_UNICODE
        );
    }
}



