<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Like;
use App\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class LikeController extends Controller
{
    /**
     * Created By MinaBazregari@ManaGroup
     * Phone Number : 09168128563
     * Date : 3/11/2020
     * Time : 10:00 PM
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function like($id)
    {
        $user = Auth::user();
        $like = Like::where('user_id', $user->id)->where('media_id', $id)->first();
        if ($like) {
            $like->delete();
            $msg = 'لایک لغو شد';
        }
        else {
            $like = new Like();
            $like->user_id = $user->id;
            $like->media_id = $id;
            $like->save();
            $msg = 'لایک شد';
        }
        return response()->json([
            'status' => 'success',
            'message' => $msg,
        ], 200,
            array('Content-Type' => 'application/json; charset=utf-8'),
            JSON_UNESCAPED_UNICODE);
    }
}
