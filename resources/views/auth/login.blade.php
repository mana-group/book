<!DOCTYPE html>
<html>
<head lang="fa">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ورود</title>

    <!-----Material Design Bootstrap Files --->
    <link rel="stylesheet" href="styles/includes/mdb/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/includes/mdb/css/mdb.min.css">

    <!------Font-awesome Fonts Css ----->
    <link rel="stylesheet" href="styles/plugins/font-awesome-4.7.0/css/font-awesome.min.css">

    <!------Fonts-Material design ----->
    <link rel="stylesheet" href="styles/plugins/material-desing-icons/material-icons.css">

    <!----Theme Styles --->
    <link rel="stylesheet" href="styles/includes/css/customize.css">
    <link rel="stylesheet" href="styles/includes/css/themes.css">
    <link rel="stylesheet" href="styles/includes/css/style.css">

</head>
<body style="background-color: #010101;">


<!---- Container ----->
<section class="container">
    @if(session()->has('error') )
        <div class="alert alert-danger" role="alert">
            <p >{{session('error')}}</p>
        </div>
    @endif
    <div class="row">
        <div class="col-lg-6 pull-lg-3 col-sm-12 col-md-8 pull-md-2">
            <center>
                <img src="{{asset('/files/site/logo.svg')}}"/>
            </center>
            <div class="card">
                <div class="card-block">
                    <form action="login" method="post">
                        @csrf
                        <p class="text-center">
                            ورود
                        </p>
                        <div class="form-group md-form">
                            <i class="fa prefix fa-user"></i>
                            <input type="text" id="username" class="form-control" name="username">
                            <label for="username">نام کاربری</label>
                        </div>
                        <div class="form-group md-form">
                            <i class="fa prefix fa-lock"></i>
                            <input type="password" id="password" class="form-control" name="password">
                            <label for="password">رمز عبور</label>
                        </div>
                        <div class="form-group">
                            <center>
                                <button type="submit" class="btn green">ورود</button>

                            </center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!--- JQuery (necessary File) --->
<script type="text/javascript" src="styles/includes/js/jquery-3.1.1.min.js"></script>

<!--Material Design Bootstrap  Js Files ---->
<script type="text/javascript" src="styles/includes/mdb/js/tether.min.js"></script>

<script type="text/javascript" src="styles/includes/mdb/js/bootstrap.min.js"></script>
<script type="text/javascript" src="styles/includes/mdb/js/mdb.min.js"></script>


<!------ Theme Script --->
<script type="text/javascript" src="styles/includes/js/admin.js"></script>


</body>
</html>