@extends("layout.sidebar")
@section('content')

    <section class="main-container">
        @if(session()->has('success') )
            <div class="alert alert-success" role="alert">
                <p>{{session('success')}}</p>
            </div>
        @endif
        @if(session()->has('error') )
            <div class="alert alert-danger" role="alert">
                <p>{{session('error')}}</p>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-block">
                        <div class="card-text">
                            <table class="table table-hover table-striped table-bordered" id="mainTable">
                                <thead>
                                <tr>
                                    <th style="text-align: center">
                                        ردیف
                                    </th>
                                    <th style="text-align: center">
                                        نام
                                    </th>
                                    <th style="text-align: center">
                                        تخلص
                                    </th>
                                    <th style="text-align: center">
                                        قالب
                                    </th>
                                    <th style="text-align: center">
                                        ویرایش
                                    </th>
                                    <th style="text-align: center">
                                        حذف
                                    </th>
                                    <th style="text-align: center">
                                        عکس
                                    </th>
                                    <th style="text-align: center">
                                        دسته بندی
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($poets as $poet)
                                    <tr>
                                        <td style="text-align: center">
                                            {{$poet->id}}
                                        </td>
                                        <td style="text-align: center">
                                            {{$poet->name}}
                                        </td>
                                        <td style="text-align: center">
                                            {{$poet->authorName}}
                                        </td>
                                        <td style="text-align: center">
                                            {{$poet->format}}
                                        </td>
                                        <td style="text-align: center">
                                            <a href="/poet/edit/{{$poet->id}}"><i class="fa fa-fw fa-pencil"
                                                                                  style="color: green"></i></a>
                                        </td>
                                        <td style="text-align: center">
                                            <a href="/poet/delete/{{$poet->id}}"><i class="fa fa-fw fa-trash"
                                                                                    style="color: darkred"></i></a>
                                        </td>
                                        <td style="text-align: center;padding-top: 40px">
                                            <img src="/files/poets{{$poet->image}}"
                                                 style="height: 100px;width: 100px ; margin: auto;">
                                        </td>
                                        <td style="text-align: center">
                                            <a href="/category/list/{{$poet->id}}"><i class="fa fa-fw fa-product-hunt"
                                                                                      style="color: darkred"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection