@extends("layout.sidebar")
@section('content')
    <section class="main-container">
        @if(session()->has('success') )
            <div class="alert alert-success" role="alert">
                <p>{{session('success')}}</p>
            </div>
        @endif
        @if(session()->has('error') )
            <div class="alert alert-danger" role="alert">
                <p>{{session('error')}}</p>
            </div>
        @endif
        @if($errors->any && $errors->first())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach( $errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post"
              action="@if(isset($poet)){{'/poet/edit/'.$poet->id}}
              @else{{'/poet/create'}}@endif" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <section class="card">
                        <section class="card-block">
                            <div class="card-title">
                                <p style="text-align: right">شاعر</p>
                                <hr>
                            </div>
                            <div class="row">
                                <div class=" col-md-4 form-group">
                                    <div class="md-form form-line">
                                        <label>نام :</label>
                                        <input type="text" class="form-control"
                                               value="@if(isset($poet)){{$poet->name}}@endif" required
                                               name="name">
                                    </div>
                                </div>
                                <div class=" col-md-4 form-group">
                                    <div class="md-form form-line">
                                        <label>تخلص :</label>
                                        <input type="text" class="form-control"
                                               value="@if(isset($poet)){{$poet->authorName}}@endif" required
                                               name="authorName">
                                    </div>
                                </div>
                                <div class=" col-md-4 form-group">
                                    <div class="md-form form-line">
                                        <label>قالب :</label>
                                        <input type="text" class="form-control"
                                               value="@if(isset($poet)){{$poet->format}}@endif" required
                                               name="format">
                                    </div>
                                </div>
                                <div class=" col-md-4 form-group">
                                    <label>عکس </label>
                                    <div class="md-form form-line">
                                        <input type="file" class="form-control"
                                               value="@if(isset($poet)){{$poet->image}}@else required @endif"
                                               name="image">
                                    </div>
                                </div>

                                <div class="col-md-4"></div>

                            </div>

                            <div class="row">

                                <div class="col-md-4"></div>
                                <div class="col-md-4 ">
                                    <button class="btn green" value="send" type="submit" name="send"
                                            style="background-color: #24b24a;width: 100%;border-radius: 10px">ثبت
                                    </button>
                                </div>
                                <div class="col-md-4"></div>
                            </div>


                        </section>
                    </section>
                </div>
            </div>
        </form>
    </section>

@endsection