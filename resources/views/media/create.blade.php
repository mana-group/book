@extends("layout.sidebar")
@section('content')
    <section class="main-container">
        @if(session()->has('success') )
            <div class="alert alert-success" role="alert">
                <p>{{session('success')}}</p>
            </div>
        @endif
        @if(session()->has('error') )
            <div class="alert alert-danger" role="alert">
                <p>{{session('error')}}</p>
            </div>
        @endif
        @if($errors->any && $errors->first())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach( $errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post"
              action="@if(!isset($media)) /media/create/{{$poetry->id}}  @else {{'/media/edit/'.$media->id}}  @endif"
              enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <section class="card">
                        <section class="card-block">
                            <div class="card-title">
                                <p style="text-align: right">رسانه</p>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class=" col-md-4 form-group">
                                    <label>نوع </label>
                                    <div class="md-form form-line">
                                        <div class="col-md-4 align-self-center">
                                            <input type="file"
                                                   value="@if(isset($media)){{$media->type}}@else required @endif"
                                                   name="file">
                                        </div>
                                        <div class=" col-md-4 form-group">
                                            <div class="md-form form-line">
                                                <select name="type">
                                                    <option value="1">video</option>
                                                    <option value="2">audio</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-4 ">
                                                <button class="btn green" value="send" type="submit" name="send"
                                                        style="background-color: #24b24a;width: 100%;border-radius: 10px">
                                                    ثبت
                                                </button>
                                            </div>
                                            <div class="col-md-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>
                </div>
            </div>
        </form>
    </section>
@endsection