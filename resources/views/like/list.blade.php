@extends("layout.sidebar")
@section('content')

    <section class="main-container">
        @if(session()->has('success') )
            <div class="alert alert-success" role="alert">
                <p>{{session('success')}}</p>
            </div>
        @endif
        @if(session()->has('error') )
            <div class="alert alert-danger" role="alert">
                <p>{{session('error')}}</p>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-block">
                        <div class="card-text">
                            <table class="table table-hover table-striped table-bordered" id="mainTable">
                                <thead>
                                <tr>
                                    <th style="text-align: center">
                                        ردیف
                                    </th>
                                    <th style="text-align: center">
                                        رسانه
                                    </th>
                                    <th style="text-align: center">
                                        ارسال
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($likes as $like)
                                    <tr>
                                        <td style="text-align: center">
                                            {{$like->id}}
                                        </td>
                                        <td style="text-align: center">
                                            {{$like->media_id}}
                                        </td>
                                        <td style="text-align: center">
                                            {{$like->user_id}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
