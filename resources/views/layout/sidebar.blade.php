<!DOCTYPE html>
<html>
<head lang="fa">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('/styles/includes/mdb/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('styles/includes/mdb/css/mdb.min.css')}}">
    <link rel="stylesheet" href="{{asset('styles/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">


    <link rel="stylesheet" href="{{asset('styles/plugins/material-desing-icons/material-icons.css')}}">


    <link rel="stylesheet" href="{{asset('styles/includes/css/customize.css')}}">
    <link rel="stylesheet" href="{{asset('styles/includes/css/themes.css')}}">
    <link rel="stylesheet" href="{{asset('styles/includes/css/style.css')}}">
    <style>
        a.HOVER:hover {
            background-color: #24b24a;
        }
    </style>
</head>
<body style="background-color: gainsboro">
@yield('content')

<nav class="navbar navbar-dark top-nav" style="background-color: black">
    <div class="nav-icon">
        <p class="text-center white-text" style="line-height: 55px;">
            <img src="{{asset('/files/site/logo.svg')}}">
        </p>
    </div>
    <div class="nav-list">
        <ul class="nav pull-right">
            <li class="nav-item active waves-effect waves-light">
                <a href="javascript:;" id="close-menu" class="nav-link"><i class="fa fa-bars"></i></a>
            </li>
        </ul>
        <ul class="nav pull-left tools_list">
            <div class="col-md-3 col-sm-3"><a href="/logout"><i class="fa fa-fw fa-sign-out"
                                                                style="font-size: 28px;padding: 5px;color: white"></i></a>
            </div>
        </ul>
    </div>
</nav>

<aside class="sidebar z-depth-1" style="background-color: black">
    <!-- <div class="profile btn-group">
         <img src="includes/img/user8-128x128.jpg" class="border-r-3 z-depth-1" />
     </div>-->
    <ul class="accordion" id="accordion" role="tablist" aria-multiselectable="false">
        <li class="HOVER header" style="background-color: #24b24a;color: white;">منو اصلی</li>
        <li class="sidebar-link">
            <a class="waves-effect" href="javascript:;" style="color: white">
               شاعران
                <span class="fa fa-angle-down"></span>
            </a>
            <ul class="collapse">
                <li class="waves-effect"><a class="HOVER" href="/poet/list" style="color: white">
                        لیست
                    </a></li>
                <li class="waves-effect"><a class="HOVER" href="/poet/create" style="color: white">
                        افزودن
                    </a></li>
            </ul>
        </li>

    </ul>
</aside>


<script type="text/javascript" src="{{asset('styles/includes/js/jquery-3.1.1.min.js')}}"></script>


<script type="text/javascript" src="{{asset('styles/includes/mdb/js/tether.min.js')}}"></script>

<script type="text/javascript" src="{{asset('styles/includes/mdb/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('styles/includes/mdb/js/mdb.min.js')}}"></script>


<script type="text/javascript" src="{{asset('styles/includes/js/admin.js')}}"></script>
</body>
</html>