@extends("layout.sidebar")
@section('content')

    <section class="main-container">
        @if(session()->has('success') )
            <div class="alert alert-success" role="alert">
                <p>{{session('success')}}</p>
            </div>
        @endif
        @if(session()->has('error') )
            <div class="alert alert-danger" role="alert">
                <p>{{session('error')}}</p>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-block">
                        <div class="card-text">
                            <table class="table table-hover table-striped table-bordered" id="mainTable">
                                <thead>
                                <tr>
                                    <th style="text-align: center">
                                        ردیف
                                    </th>
                                    <th style="text-align: center">
                                        نام
                                    </th>
                                    <th style="text-align: center">
                                        ویرایش
                                    </th>
                                    <th style="text-align: center">
                                        حذف
                                    </th>
                                    <th style="text-align: center">
                                        اشعار
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td style="text-align: center">
                                            {{$category->id}}
                                        </td>
                                        <td style="text-align: center">
                                            {{$category->name}}
                                        </td>
                                        <td style="text-align: center">
                                            <a href="/category/edit/{{$category->id}}"><i class="fa fa-fw fa-pencil" style="color: green"></i></a>
                                        </td>
                                        <td style="text-align: center">
                                            <a href="/category/delete/{{$category->id}}"><i class="fa fa-fw fa-trash" style="color: darkred"></i></a>
                                        </td>
                                        <td style="text-align: center">
                                            <a href="/poetry/list/{{$category->id}}"><i class="fa fa-fw fa-book" style="color: darkred"></i></a>
                                        </td>
                                        {{--<td style="text-align: center">--}}
                                            {{--<a href="/poetry/list/{{$category->id}}"><i class="fa fa-fw fa-trash" style="color: darkred"></i></a>--}}
                                        {{--</td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="row">

                                <div class="col-md-4"></div>
                                <div class="col-md-4 ">
                                <a href="/category/create/{{$poet->id}}"><button class="btn green" value="send" type="submit" name="send"
                                style="background-color: #24b24a;width: 100%;border-radius: 10px">افزودن
                                </button>
                                </a>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection