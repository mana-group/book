@extends("layout.sidebar")
@section('content')
    <section class="main-container">
        @if(session()->has('success') )
            <div class="alert alert-success" role="alert">
                <p>{{session('success')}}</p>
            </div>
        @endif
        @if(session()->has('error') )
            <div class="alert alert-danger" role="alert">
                <p>{{session('error')}}</p>
            </div>
        @endif
        @if($errors->any && $errors->first())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach( $errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post"
              action="@if(isset($poetry)){{'/poetry/edit/'.$poetry->id}}
              @else{{'/poetry/create/'. $category->id}}@endif" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <section class="card">
                        <section class="card-block">
                            <div class="card-title">
                                <p style="text-align: right">شعر</p>
                                <hr>
                            </div>

                            <div class="row">
                                <div class=" col-md-4 form-group">
                                    <div class="md-form form-line">
                                        <label>نام :</label>
                                        <input type="text" class="form-control"
                                               value="@if(isset($poetry)){{$poetry->name}}@endif" required
                                               name="name">
                                    </div>
                                </div>
                                <div class=" col-md-4 form-group">
                                    <div class="md-form form-line">
                                        <label>متن :</label>
                                        <textarea name="text"
                                                  required>@if(isset($poetry)){{$poetry->text}}@endif</textarea>
                                    </div>
                                </div>
                                <div class=" col-md-4 form-group">
                                    <div class="col-md-4 form-group">
                                        <button class="btn green" value="send" type="submit" name="send"
                                                style="background-color: #24b24a;border-radius: 10px">ثبت
                                        </button>
                                    </div>
                                </div>
                            </div>


                        </section>
                    </section>
                </div>
            </div>
        </form>
    </section>

@endsection